﻿using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;

namespace Otus.Teaching.Pcf.ReferenceBook.Core.Abstractions.Repositories
{

    public interface IRepository<T>
        where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}