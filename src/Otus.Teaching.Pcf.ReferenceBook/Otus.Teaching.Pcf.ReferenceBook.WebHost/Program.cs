using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.ReferenceBook.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReferenceBook.DataAccess.Data;
using Otus.Teaching.Pcf.ReferenceBook.DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
//builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

builder.WebHost.UseUrls("http://*:80");
builder.Services.AddControllers();


builder.Services.AddDbContext<DataContext>(x => 
    {

        x.UseNpgsql(builder.Configuration.GetConnectionString("ReferenceBookDb"));
        x.UseSnakeCaseNamingConvention();
        x.UseLazyLoadingProxies();
    });


// ����������� � Redis
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetConnectionString("RedisConnectionParams");
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// ������������� ��
var scope = app.Services.CreateScope();
var basectx = scope.ServiceProvider.GetRequiredService<DataContext>();
IDbInitializer dbInitializer = new EfDbInitializer(basectx);
dbInitializer.InitializeDb();


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();
