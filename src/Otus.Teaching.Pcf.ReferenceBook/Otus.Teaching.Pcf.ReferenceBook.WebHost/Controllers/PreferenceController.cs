using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.ReferenceBook.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;
using Otus.Teaching.Pcf.ReferenceBook.WebHost.DTO;

namespace Otus.Teaching.Pcf.ReferenceBook.WebHost.Controllers
{
    /// <summary>
    /// ������������ ��������
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IDistributedCache _distributedCache;

        public PreferencesController(IRepository<Preference> preferencesRepository, IDistributedCache distributedCache)
        {
            _preferencesRepository = preferencesRepository;
            _distributedCache = distributedCache;
        }

        /// <summary>
        /// �������� ������ ������������
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PreferenceResponse>>> GetPreferencesAsync()
        {
            var preferences = await _preferencesRepository.GetAllAsync();

            var response = preferences.Select(x => new PreferenceResponse()
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();

            return Ok(response);
        }


        /// <summary>
        /// �������� ������������ �� ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<PreferenceResponse>> GetPreferencesByIdAsync(Guid id)
        {
            var preferences = await _preferencesRepository.GetByIdAsync(id);
            if (preferences == null)
            {
                return NotFound();
            }
            var response = new PreferenceResponse() { Id = preferences.Id, Name = preferences.Name };

            // �������, ���� ���������� � ������������ �������� - ���� ��������� ��� Redis
            var redis = await _distributedCache.GetAsync(response.Id.ToString());
            if (redis == null)
            {
                await _distributedCache.SetStringAsync(response.Id.ToString(), response.Name);
            }


            return Ok(response);
        }


    }

}