﻿using System;

namespace Otus.Teaching.Pcf.ReferenceBook.WebHost.DTO
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}