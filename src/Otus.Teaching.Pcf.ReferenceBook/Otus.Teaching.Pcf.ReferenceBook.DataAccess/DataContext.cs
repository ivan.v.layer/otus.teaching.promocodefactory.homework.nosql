﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.ReferenceBook.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReferenceBook.DataAccess.Data
{
    public class DataContext
            : DbContext
    {
        public DbSet<Preference> Preferences { get; set; }


        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }
}
