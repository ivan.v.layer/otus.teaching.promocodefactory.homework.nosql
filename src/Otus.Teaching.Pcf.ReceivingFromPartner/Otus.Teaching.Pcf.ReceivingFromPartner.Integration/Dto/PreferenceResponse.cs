﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}