﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Gateways;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class ReferenceBookGateway : IReferenceBookGateway
    {
        private readonly HttpClient _httpClient;

        public ReferenceBookGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<PreferenceResponse> GetPreferenceByIdAsync(Guid preferenceGuid)
        {
            

            var response = await _httpClient.GetAsync($"api/v1/preferences/{preferenceGuid}");

            if (response.IsSuccessStatusCode)
            {
                return response.Content.ReadAsAsync<PreferenceResponse>().Result;
            }
            else
            {
                return null;
            }

        }
    }
}