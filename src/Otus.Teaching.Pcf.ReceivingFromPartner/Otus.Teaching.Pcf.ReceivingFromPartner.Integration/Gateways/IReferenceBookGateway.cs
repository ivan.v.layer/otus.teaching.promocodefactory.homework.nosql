﻿using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Gateways
{
    public interface IReferenceBookGateway
    {
        Task<PreferenceResponse> GetPreferenceByIdAsync(Guid preferenceGuid);
    }
}

