﻿using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}