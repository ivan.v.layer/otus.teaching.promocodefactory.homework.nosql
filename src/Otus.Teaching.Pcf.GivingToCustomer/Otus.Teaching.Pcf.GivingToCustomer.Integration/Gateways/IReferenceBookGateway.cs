﻿using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using System;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.GivingToCustomer.Integration.Gateways
{
    public interface IReferenceBookGateway
    {
        Task<PreferenceResponse> GetPreferenceByIdAsync(Guid preferenceGuid);
    }
}

