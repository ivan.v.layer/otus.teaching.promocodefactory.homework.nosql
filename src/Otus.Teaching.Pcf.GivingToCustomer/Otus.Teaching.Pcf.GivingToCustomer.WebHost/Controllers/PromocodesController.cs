﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Dto;
using Otus.Teaching.Pcf.GivingToCustomer.Integration.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly ILogger<PromocodesController> _log;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly IDistributedCache _distributedCache;
        private readonly IReferenceBookGateway _referenceBookGateway;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, IRepository<Customer> customersRepository,
            IDistributedCache distributedCache,
            ILogger<PromocodesController> log,
            IReferenceBookGateway referenceBookGateway)
        {
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
            _distributedCache = distributedCache;
            _log = log;
            _referenceBookGateway = referenceBookGateway;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promocodes = await _promoCodesRepository.GetAllAsync();

            var response = promocodes.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerId = x.PartnerId,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //Получаем предпочтение по имени

            // Получаем предпочтение из кеша
            Preference preference = null;

            Stopwatch redisStopwatch = Stopwatch.StartNew();
            var fromRedis = await _distributedCache.GetAsync(request.PreferenceId.ToString());
            redisStopwatch.Stop();
            if (fromRedis != null)
            {
                string preferenceName = Encoding.UTF8.GetString(fromRedis);
                preference = new Preference() { Id = request.PreferenceId, Name = preferenceName };
                _log.LogInformation($"PreferencId {request.PreferenceId} с значением {preferenceName} получен из кеша. Потребовалось времени: {redisStopwatch.ElapsedMilliseconds} ms");
            }
            else // В кеше не найдено - получаем напрямую от микросервиса ReferenceBook
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                PreferenceResponse httpResponse = await _referenceBookGateway.GetPreferenceByIdAsync(request.PreferenceId);
                stopwatch.Stop();
                if (httpResponse != null)
                {
                    preference = new Preference() { Id = httpResponse.Id, Name = httpResponse.Name };
                    _log.LogInformation($"PreferencId {request.PreferenceId} с значением {httpResponse.Name} получен мимо кеша. Потребовалось времени: {stopwatch.ElapsedMilliseconds} ms");
                }
            }



            if (preference == null)
            {
                return BadRequest();
            }

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customersRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            PromoCode promoCode = PromoCodeMapper.MapFromModel(request, preference, customers);

            await _promoCodesRepository.AddAsync(promoCode);

            return CreatedAtAction(nameof(GetPromocodesAsync), new { }, null);
        }
    }
}